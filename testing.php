<?php
/**
 * Plugin Name: Testing Plugin
 * Plugin URI: https://bitbucket.org/plugindrawer/testing
 * Bitbucket Plugin URI: https://bitbucket.org/plugindrawer/testing
 * Description: Just a test.
 * Version: 0.2
 * Author: Plugin Drawer
 * Author URI: http://plugindrawer.com
 * License: GPL2
 */
 
 add_action('wp_head', 'test_function');

function test_function() {
	echo 'The auto updater is working! This is the updated version from Bitbucket repo.';
}
